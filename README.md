# NodeMCU-DS18B20-MQTT
This is meant to be an IOT device that notifies an MQTT broker of the temperature at a set interval

**WARNING**

I do not accept any blame for issues that may arise from the use of this code.

##How to set up
Items needed:

* 1x ES8266 Microcontroller
* 1x DS18B20 Temperature Sensor
* 1x 4.7K Pullup resistor
* Enough cables to connect it all!

![Diagram](https://raw.githubusercontent.com/mattysmith22/ESP8266-MQTT-Temperature-sensor/master/Pictures/NodeMCU.png)

##Dependencies
This was created using the ESP8266 libraries for the arduino IDE. They are located at: <https://github.com/esp8266/Arduino>

The following libraries were also used:
PubSubClient <https://github.com/knolleary/pubsubclient>
OneWire <http://www.pjrc.com/teensy/td_libs_OneWire.html>
DallasTemperature <https://github.com/milesburton/Arduino-Temperature-Control-Library>